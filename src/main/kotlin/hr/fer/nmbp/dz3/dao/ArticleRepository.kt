package hr.fer.nmbp.dz3.dao

import hr.fer.nmbp.dz3.domain.Article
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ArticleRepository: MongoRepository<Article, String>