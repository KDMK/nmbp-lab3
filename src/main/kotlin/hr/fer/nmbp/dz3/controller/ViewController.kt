package hr.fer.nmbp.dz3.controller

import hr.fer.nmbp.dz3.dao.ArticleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.util.stream.Collectors
import java.util.stream.IntStream


@Controller
@RequestMapping("/article")
class  ViewController @Autowired constructor(var articleRepository: ArticleRepository) {

    @GetMapping
    fun listAllArticles(@RequestParam(defaultValue = "0", required = false) page: String, model: Model): String {
        val articles = articleRepository.findAll(PageRequest.of(page.toInt(), 10))

        val totalPages = articles.totalPages
        if (totalPages > 0) {
            val pageNumbers = IntStream.rangeClosed(0, totalPages - 1)
                    .boxed()
                    .collect(Collectors.toList())
            model.addAttribute("pageNumbers", pageNumbers)
        }

        model.addAttribute("articles", articles)
        model.addAttribute("currentPage", articles.number)
        model.addAttribute("isFirst", articles.isFirst)
        model.addAttribute("isLast", articles.isLast)
        model.addAttribute("totalPages", totalPages)

        return "article/list"
    }

    @GetMapping("/{id}")
    fun listSigleArticle(@PathVariable id: String, model: Model): String {
        val article = articleRepository.findById(id)

        model.addAttribute("article", article.get())
        return "article/single"
    }
}