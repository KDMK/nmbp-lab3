package hr.fer.nmbp.dz3.controller

import hr.fer.nmbp.dz3.dao.ArticleRepository
import hr.fer.nmbp.dz3.domain.Article
import hr.fer.nmbp.dz3.domain.Comment
import hr.fer.nmbp.dz3.domain.dto.CommentDTO
import org.apache.logging.log4j.LogManager.getLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import java.util.*

@RestController
@RequestMapping("/api/article")
class ArticleController @Autowired constructor(var articleRepository: ArticleRepository) {

    companion object {
        private val log: org.apache.logging.log4j.Logger = getLogger(ArticleController.javaClass)
    }

    @GetMapping
    fun getAllArticles(): ResponseEntity<List<Article>> {
        return ResponseEntity.ok(articleRepository.findAll())
    }

    @GetMapping("/{id}")
    fun getSingleArticle(@PathVariable id: String): ResponseEntity<Optional<Article>> {
        return ResponseEntity.ok(articleRepository.findById(id))
    }

    @PostMapping("/comment/{id}")
    fun postComment(@PathVariable id: String, @RequestBody commentDTO: CommentDTO) {
        val article = articleRepository.findById(id).orElseGet(null)

        if (article != null) {
            if (article.comments == null)
                article.comments = ArrayList()
            val comment = Comment(commentDTO.body, commentDTO.author, LocalDateTime.now())
            article.comments?.add(comment)
            articleRepository.save(article)
        }
    }
}