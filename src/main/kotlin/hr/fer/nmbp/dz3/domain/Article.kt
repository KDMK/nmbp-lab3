package hr.fer.nmbp.dz3.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection="articles")
data class Article(
        @Id var _id: String,
        var title: String?,
        var body: String?,
        var img: String,
        var author: Author,
        var comments: MutableList<Comment?>?
)