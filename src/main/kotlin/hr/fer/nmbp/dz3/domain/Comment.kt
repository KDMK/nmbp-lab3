package hr.fer.nmbp.dz3.domain

import java.time.LocalDateTime

data class Comment(
        var body: String,
        var author: String,
        var created: LocalDateTime?
)