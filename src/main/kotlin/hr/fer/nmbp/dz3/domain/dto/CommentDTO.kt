package hr.fer.nmbp.dz3.domain.dto

data class CommentDTO(
        var body: String,
        var author: String
)