package hr.fer.nmbp.dz3.domain

data class Author(
        var fullname: String?,
        var email: String
)