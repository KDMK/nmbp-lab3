package hr.fer.nmbp.dz3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Dz3Application

fun main(args: Array<String>) {
	runApplication<Dz3Application>(*args)
}

