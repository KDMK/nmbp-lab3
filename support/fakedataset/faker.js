let faker = require('faker')

let authors = []

let randomName
let radnomEmail

for (let i=0; i < 2347; i++) {
  randomName = faker.name.findName()
  randomEmail = faker.internet.email()

  let author = {
    fullname: randomName,
    email: randomEmail
  }

  authors.push(author)
}

let articles = []
let articleTitle
let articleBody
let articleImage
for (let i=0; i < 21456; i++) {
  articleTitle = faker.hacker.phrase()
  articleBody = faker.lorem.paragraph()
  articleImage = faker.image.image()

  let article = {
    title: articleTitle,
    body: articleBody,
    img: articleImage,
    author: authors[(Math.random() * 2347) << 0]  
  }

  articles.push(article)
}

let fs = require('fs')
fs.writeFile('articles.json', JSON.stringify(articles), 'utf8');
