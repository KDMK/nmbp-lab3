let mongo = require('mongodb')
let MongoClient = mongo.MongoClient
let databaseUrl = 'mongodb://root:example@localhost:27017/?authMechanism=SCRAM-SHA-1&authSource=admin'

var fs = require('fs');
var articles = JSON.parse(fs.readFileSync('articles.json', 'utf8'));

MongoClient.connect(databaseUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("nmbp_project")
    dbo.createCollection("articles", function(err, res) {
          if (err) throw err;
    });
    dbo.collection('articles').insertMany(articles, (err, res) => {
      if (err) throw err
      db.close()
    })
});
